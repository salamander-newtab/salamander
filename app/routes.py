"""Setup for salamander page(s)."""

from flask import render_template
from app import app
from datetime import datetime
from .config import my_modules
from flask import Markup

@app.route('/')
@app.route('/index')
def index():
	"""Homepage for salamander."""
	output = []
	for m in my_modules["home"]:
		output.append( Markup(m.display()) )

	return render_template('index.html', title='Home', theme=my_modules["theme"](), output=output)
