"""Link class for salamander."""

from .label import Label


class Link(Label):
	def __init__(self, icon, icon_type, title, url):
		super().__init__(icon, icon_type, title)
		self.url = url

	def display(self):
		outstr = '<tr><td class="{itype}"><a href="{u}">{i}</a></td><td><a href="{u}">{t}</a></td></tr>'
		return outstr.format(itype=self.icon_type, u=self.url, i=self.icon, t=self.title)
