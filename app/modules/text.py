"""Plain text module for salamander."""

from .module import Salamander_Module


class Text(Salamander_Module):
	def __init__(self, text):
		self.text = text

	def display(self):
		return self.text
