"""A list of links."""

from .module import Salamander_Module
from .label import Label
from .link import Link


class Chain(Salamander_Module):
	"""A list of links under a given category."""

	def __init__(self):
		"""Initialize a chain."""
		self.chain = []

	def __add__(self, other):
		"""Add a link or label to the chain."""
		self.chain.append(other)
		return self

	def display(self):
		"""Prepare the chain for output to salamander."""
		output = "<table>"

		for entry in self.chain:
			output += entry.display()

		output += "</table>"

		return output
