"""Set theme based on time."""

from datetime import datetime

day_theme = "solarizedLight"
night_theme = "solarizedDark"

day_hour = 8
night_hour = 20

def get_theme():
	hour = int(datetime.now().strftime('%H'))
	if hour > day_hour and hour < night_hour:
		return day_theme
	else:
		return night_theme