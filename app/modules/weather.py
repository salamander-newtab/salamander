"""Module to display current weather information, data is provided by
OpenWeatherMap using the PyOWM library.  Please put your API key in a file
located in the modules folder called api_keys.py in a variable called OWM_KEY"""

from .api_keys import OWM_KEY
from .module import Salamander_Module
from pyowm import OWM

class Weather(Salamander_Module):

	def __init__(self, cityID, scale):
		self.weather_obj = OWM(OWM_KEY)
		self.city = cityID
		self.scale = scale
		self.icons = {	'01d': '', '01n': '',
						'02d': '', '02n': '',
						'03d': '', '03n': '',
						'04d': '', '04n': '',
						'09d': '', '09n': '',
						'10d': '', '10n': '',
						'11d': '', '11n': '',
						'13d': '', '13n': '',
						'50d': '', '50n': ''}

	def display(self):
		observation = self.weather_obj.weather_manager().weather_at_id(self.city)
		weather = observation.weather
		city = observation.location.name
		wIcon = self.icons[weather.weather_icon_name]
		if self.scale == 'F' or self.scale == 'f':
			temperature = weather.temperature('fahrenheit')['temp']
		elif self.scale == 'C' or self.scale == 'c':
			temperature = weather.temperature(unit='celsius')['temp']

		return '''<table class="weather"><tr><th colspan="2">Weather for {city}:</th></tr>
					<tr><td class="weatherIcon">{wIcon}</td>
					<td class="weatherTemp">{temp}°{unit}</td></tr>
					</table>'''.format(city=city, wIcon=wIcon, temp=int(temperature),
										unit=self.scale)
