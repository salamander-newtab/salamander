"""Label module for salamander."""

from .module import Salamander_Module


class Label(Salamander_Module):
	def __init__(self, icon, icon_type, title):
		self.icon = icon
		self.icon_type = icon_type
		self.title = title

	def display(self):
		outstr = '<tr><th class="{itype}">{i}</th><th>{t}</th></tr>'
		return outstr.format(itype=self.icon_type, i=self.icon, t=self.title)
