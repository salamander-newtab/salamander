"""Welcomes user(name)."""

from .module import Salamander_Module
import getpass


class Welcome(Salamander_Module):
	def __init__(self, greeting):
		self.user = getpass.getuser()
		self.greeting = greeting

	def display(self):
		return '<h1>' + self.greeting +', ' + self.user + '!</h1>'