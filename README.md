# salamander

A modular, browser-agnostic *newt*ab page

## Installation instructions for Linux:
1. Clone this repository
1. Change directory to the salamander folder in your terminal
1. Enter `python3 -m venv venv_salamander` to activate the python part of the virtual environment.
1. Enter `source venv_salamander/bin/activate` to activate the virtual environment.
1. Enter `flask run` to start salamander
1. Set your browser(s)'s new tab page to `http://localhost:5000`
1. ???
1. Profit